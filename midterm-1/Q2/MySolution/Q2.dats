(*
//
** Midterm
//
** Course: BU CAS CS520
// Term: Spring 2020
** Name: Andreas Singer
** Email: azsinger@bu.edu
//
*)

(* ****** ****** *)

staload "./prop-logic.sats"

(* ****** ****** *)
//
// 10 points
//
extern
prfun
Contrapos{A,B,C:prop}
  (pf: PIMPL(A, B)): PIMPL(PIMPL(B, C), PIMPL(A, C))

primplement
Contrapos{A,B,C:prop}(pf) =
  impl_intr(
    lam pf1 =>
    impl_intr(
      lam pf2 =>
      impl_elim(pf1, impl_elim(pf, pf2))
    )
  )
  
  
//
(* ****** ****** *)
//
// 05 points
//
extern
prfun
Konstant{A,B:prop}
  (pf: A): PIMPL(B, A)

primplement
Konstant{A,B}(pf) =
  impl_intr(
    lam pf1 => pf
  )
  
//
// 10 points
//
extern
prfun
Substitute{A,B,C:prop}
  (pf: PIMPL(A, PIMPL(B, C))): PIMPL(PIMPL(A, B), PIMPL(A, C))

primplement
Substitute{A,B,C : prop}(pf) =
  impl_intr(
    lam pf1 =>
    impl_intr(
      lam pf2 =>
      impl_elim(impl_elim(pf, pf2), impl_elim(pf1, pf2))
    )
  )


//
(* ****** ****** *)

implement main0 () = ()

(* end of [Q2.dats] *)



