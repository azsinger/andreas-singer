(*
//
** Midterm
//
** Course: BU CAS CS520
// Term: Spring 2020
** Name: Andreas Singer
** Email: azsinger@bu.edu
*)

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
(* ****** ****** *)
//
datatype
mylist(a:t@ype+, int) =
//
| mylist_nil(a, 0) of ()
//
| {n:pos}
  mylist_cons0(a, n+n) of (mylist(a, n), mylist(a, n))
| {n:nat}
  mylist_cons1(a, n+n+1) of (a, mylist(a, n), mylist(a, n))

//
(* ****** ****** *)
//
// HX: 10 points
// [mylist_cons] is O(log(n))-time
//


extern
fun
{a:t@ype}
mylist_cons{n:nat}
  (x: a, xs: mylist(INV(a), n)): mylist(a, n+1)

implement
{a}
mylist_cons{n}(x, xs) =
  case+ xs of
  | mylist_nil()               => mylist_cons1(x, mylist_nil(), mylist_nil())
  | mylist_cons0(xs1, xs2)     => mylist_cons1(x, xs1, xs2)
  | mylist_cons1(x1, xs1, xs2) => mylist_cons1(x, xs, mylist_nil())
  
  
//
(* ****** ****** *)
//
// HX: 10 points
// [mylist_uncons] is O(log(n))-time
//
extern
fun
{a:t@ype}
mylist_uncons
  {n:pos}(xs: mylist(INV(a), n)): (a, mylist(a, n-1))

implement
{a}
mylist_uncons{n}(xs) =
  case+ xs of
  | mylist_cons0(xs1, xs2)    => mylist_uncons(xs1)
  | mylist_cons1(x, xs1, xs2) => (x, mylist_cons0(xs1, xs2))
  (* | mylist_cons(x, xs1)       => (x, xs) *)
//
(* ****** ****** *)
//
// You implementation should satisfy the following
// property:
//
//   mylist_uncons(mylist_cons(x, xs)) = (x, xs)
//
(* ****** ****** *)
//
// HX:
// You may use the following implemented function
// templates for testing
//
(* ****** ****** *)
//
extern
fun
{a:t@ype}
list2mylist{n:nat}(xs: list(INV(a), n)): mylist(a, n)
//
implement
{a}(*tmp*)
list2mylist(xs) =
(
case+ xs of
| list_nil() => mylist_nil()
| list_cons(x, xs) => mylist_cons(x, list2mylist(xs))
)
(* ****** ****** *)
//
extern
fun
{a:t@ype}
mylist2list{n:nat}(xs: mylist(INV(a), n)): list(a, n)
//
implement
{a}(*tmp*)
mylist2list(xs) =
(
case+ xs of
| mylist_nil() => list_nil()
| _(*non-empty*) =>> let
    val (x, xs) = mylist_uncons(xs) in list_cons(x, mylist2list(xs))
  end // end of [mylist2list]
)
(* ****** ****** *)

implement main0 () = ()
(* end of [Q5.dats] *)

