
(*
#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
#include "share/atspre_staload_libats_ML.hats"
*)
#include "share/atspre_define.hats"
#define ATS_MAINATSFLAG 1
#define ATS_DYNLOADNAME "QuickSort__dynload"
#define ATS_STATIC_PREFIX "QuickSort__"

#define LIBATSCC2JS_targetloc "$PATSHOME/contrib/libatscc2js/ATS2-0.3.2"

#staload UN = "prelude/SATS/unsafe.sats"
#include "{$LIBATSCC2JS}/staloadall.hats"

#define HTML_targetloc "$PATSCONTRIB/contrib/HTML-emscripten"
#staload "{$HTML}/SATS/document.sats"
//#staload "{$HTML}/canvas-2d/SATS/canvas2d.sats"
#staload "{$LIBATSCC2JS}/SATS/HTML/canvas-2d/canvas2d.sats"
//#staload "{$LIBATSCC2JS}/SATS/print.sats"

#define N 30
(*
val testArray = 
(arrszref)$arrpsz{double}(13.0,22.0,16.0,18.0,15.0,21.0,27.0,14.0,3.0,2.0,20.0,5.0,4.0,10.0,0.0,9.0,28.0,1.0,6.0,23.0,8.0,24.0,19.0,7.0,11.0,26.0,12.0,17.0,25.0,29.0)
*)

val testArray = arrszref_make_arrayref(A, N) where
{
val A =
arrayref_make_elt{double}(N, 0.0)
val () =
arrayref_foreach_cloref(A, N, lam(i) => A[i] := JSmath_random())
}


abstype point_type = ptr
typedef point = point_type
abstype vector_type = ptr
typedef vector = vector_type

(* ****** ****** *)
//
extern
fun
point_make_xy
  (x: double, y: double) : point
//
(* ****** ****** *)
//
extern
fun
point_get_x (point): double
and
point_get_y (point): double
//
overload .x with point_get_x
overload .y with point_get_y
//  
(* ****** ****** *)

local
//
assume
point_type =
  $tup(double, double)
//
in (*in-of-local*)
//
implement
point_make_xy (x, y) = $tup(x, y)
//
implement point_get_x (p) = p.0
implement point_get_y (p) = p.1
//
end // end of [local]

(* ****** ****** *)
//
extern
fun
vector_make_xy
  (vx: double, vy: double): vector
//
(* ****** ****** *)
//    
extern
fun
vector_get_x (vector): double
and
vector_get_y (vector): double
//
overload .x with vector_get_x
overload .y with vector_get_y
//    
(* ****** ****** *)

local
//
assume
vector_type =
  $tup(double, double)
//
in (*in-of-local*)
//
implement
vector_make_xy (x, y) = $tup(x, y)
//
implement vector_get_x (v) = v.0
implement vector_get_y (v) = v.1
//
end // end of [local]

(* ****** ****** *)
//
extern
fun
add_pvp (point, vector): point
extern
fun
sub_ppv (p1: point, p2: point): vector
//
overload + with add_pvp
overload - with sub_ppv
//
(* ****** ****** *)
//
implement
add_pvp (p, v) =
  point_make_xy (p.x() + v.x(), p.y() + v.y())
implement
sub_ppv (p1, p2) =
  vector_make_xy (p1.x() - p2.x(), p1.y() - p2.y())
//
(* ****** ****** *)
//
extern
fun
add_vvv (v1: vector, v2: vector): vector
and
sub_vvv (v1: vector, v2: vector): vector
//
(* ****** ****** *)
//
implement
add_vvv (v1, v2) =
  vector_make_xy (v1.x() + v2.x(), v1.y() + v2.y())
implement
sub_vvv (v1, v2) =
  vector_make_xy (v1.x() - v2.x(), v1.y() - v2.y())
//
(* ****** ****** *)
//
extern
fun
mul_kvv (k: double, v: vector): vector
and
div_vkv (v: vector, k: double): vector
//
overload + with add_vvv
overload - with sub_vvv
overload * with mul_kvv
overload / with div_vkv
//
(* ****** ****** *)
//
implement
mul_kvv (k, v) = vector_make_xy (k * v.x(), k * v.y())
implement
div_vkv (v, k) = vector_make_xy (v.x() / k, v.y() / k)

local
//
val theP1 = ref{point}($UN.cast{point}(0))
val theP2 = ref{point}($UN.cast{point}(0))
val theP3 = ref{point}($UN.cast{point}(0))
val theP4 = ref{point}($UN.cast{point}(0))
//
in (* in-of-local *)
//
extern
fun theP1_get (): point 
extern
fun theP2_get (): point 
extern
fun theP3_get (): point 
extern
fun theP4_get (): point
//
extern
fun theP1_set (x: double, y: double): void = "mac#"
extern
fun theP2_set (x: double, y: double): void = "mac#"
extern
fun theP3_set (x: double, y: double): void = "mac#"
extern
fun theP4_set (x: double, y: double): void = "mac#"
//
implement
theP1_get () = theP1[]
implement
theP2_get () = theP2[]
implement
theP3_get () = theP3[]
implement
theP4_get () = theP4[]
//
implement
theP1_set (x, y) = theP1[] := point_make_xy (x, y)
implement
theP2_set (x, y) = theP2[] := point_make_xy (x, y)
implement
theP3_set (x, y) = theP3[] := point_make_xy (x, y)
implement
theP4_set (x, y) = theP4[] := point_make_xy (x, y)

end
//
(* ****** ****** *)


(*************************************************)
(* START: Direct-style Quicksort Implementation: *)
(*************************************************)
extern
fun
{a:t@ype}
swap(A :arrszref(a), i :int, j :int) : void = "mac#"

implement{a}
swap(A, i, j) =
  let val tmp = A[i] in A[i] := A[j]; A[j] := tmp
end

extern
fun
{a:t@ype}
quicksort (A :arrszref(a)) : void

extern 
fun
{a:t@ype}
sort1 (A :arrszref(a), i :int, j :int) : void

extern
fun
{a:t@ype}
sort2 (A :arrszref(a), i :int, j :int) : void // j - i >= 2

extern
fun
{a:t@ype}
sort3 (A :arrszref(a), i :int, j :int) : void

extern
fun
{a:t@ype}
partition (A :arrszref(a), i :int, j :int, pivot :a) : int


implement
{a}
quicksort(A) = 
(*
  let
  val n = g0uint2int_size_int(A.size()) in sort3 (A, 0, n)
end
*)
sort3 (A, 0, N)

(* implement
{a}
sort1 (A, i, j) =
  if j - i >= 2 then sort2 (A, i, j) else ()

implement
{a}
sort2 (A, i, j) = 
  let
  val k = i + (j - i) / 2
  val pivot = A[k]
  val () = swap (A, k, j - 1)
  val k2 = partition (A, i, j - 1, pivot)
  val () = swap (A, k2, j - 1)
  val () = sort1 (A, i, k2)
  val () = sort1 (A, k2 + 1, j)
in
end *)

implement
{a}
sort3(A, i, j) =
if j - i >= 2 then let
  val k = i + (j-i) / 2
  val pivot = A[k]
  val () = swap (A, k, j-1)
  val k2 = partition (A, i, j-1, pivot)
  val () = swap<a>(A, k2, j-1)
  val () = sort3(A, i, k2)
  val () = sort3(A, k2+1, j)
  in
  end
else ()

implement
{a}
partition (A, i, j, pivot) =
let 
fun loop ( i1 :int, i2 :int) : int =
(
  if i2 < j then
    
    if $UN.cast2int(A[i2]) < $UN.cast2int(pivot) then let
      val () = swap<a>(A, i1, i2)
    in
      loop (i1 + 1, i2 + 1)
    end else loop (i1, i2 + 1)
  else i1
)
in
  loop (0, 0)
end

(*******************************)
(* END: Direct-style Quicksort *)
(*******************************)

(* Function for printing an array of ints *)
fun
print_intarray
(A :arrszref(int)): void = let
(*
val asz =
  g0uint2int_size_int(A.size()) 
*)

fun loop (i :int, sep :string): void =
  if i < N then
    (if i > 0 then print sep; print A[i]; loop (i+1, sep))
in
  loop (0, ", ")
end

(****************************************)
(* START: CPS Quicksort Implementation: *)
(****************************************)
extern
fun{a:t@ype}
k_swap 
(A :arrszref(a), i :int, j :int, k0 :cfun(void)) : void

extern
fun{a:t@ype}
k_quicksort
(A :arrszref(a), k0 :cfun(void)) : void

extern
fun{a:t@ype}
k_aux (A :arrszref(a), i :int, j :int, k0 :cfun(void)) : void

implement
{a}
k_quicksort (A, k0) =
(*
let
val a_size = g0uint2int_size_int(A.size())
in
k_aux(A, 0, a_size, k0)
end
*)
k_aux(A, 0, N, k0)

implement
{a}
k_aux (A, i, j, k0) =
if (j - i) >= 2 then let
  val ind1 = i + (j-i) / 2
  val pivot = A[ind1]
  val () = k_swap<a>(A, ind1, j-1, k0) 
  val ind2 = partition(A, i, j-1, pivot)
  val () = k_swap<a>(A, ind2, j-1, k0)
  val () = k_aux(A, i, ind2, lam() => k_aux(A, ind2+1, j, k0))
  in
  end
else
k0()

(* k_swap: CPS version of swap *)
implement
{a}
k_swap (A, i, j, k0) = 
let
  val () = swap<a>(A, i, j)
in
end

(**********************)
(* END: CPS Quicksort *)
(**********************)

(****************************)
(* START: Drawing Functions *)
(****************************)

(*
val myArray = (arrszref)$arrpsz{int}(13,22,16,18,15,21,27,14,3,2,20,5,4,10,0,9,28,1,6,23,8,24,19,7,11,26,12,17,25,29)
*)

extern
fun
theCtx2d_get() : canvas2d = "mac#"


extern
fun
draw_theArray(ctx: canvas2d): void = "mac#"


extern
fun{
} mydraw_bargraph
(
  n: intGte(1)
, p1: point, p2: point, p3: point, p4: point
) : void // end of [mydraw_bargraph]
//
extern
fun{}
mydraw_bargraph$fcell
(
  i: intGte(0)
, p1: point, p2: point, p3: point, p4: point
) : void // end-of-function


implement
{}(*tmp*)
mydraw_bargraph
  (n, p1, p2, p3, p4) = let
//
val a = 1.0 / n
val v12 = a * (p2 - p1)
val v43 = a * (p3 - p4)
//
prval
[n:int]
EQINT() = eqint_make_gint (n)
//
fun loop
  {i:nat | i < n}
(
  i: int (i), p1: point, p4: point
) : void = let
//
val p1_new = p1 + v12
val p4_new = p4 + v43
//
val () =
mydraw_bargraph$fcell (i, p1, p1_new, p4_new, p4)
//
val i1 = i + 1
//
in
  if i1 < n then loop (i1, p1_new, p4_new) else ()
end // end of [loop]
//
in
  loop (0, p1, p4)
end // end of [mydraw_bargraph]

implement
draw_theArray
  (ctx) = let
//draw_theArray
val p1 = theP1_get()
val p2 = theP2_get()
val p3 = theP3_get()
val p4 = theP4_get()
//
implement
mydraw_bargraph$fcell<>
(
  i, p1, p2, p3, p4
) = let
//
val i =
$UN.cast{natLt(N)}(i)
val a = testArray[i]
//
macdef
floor = JSmath_floor
//clear
val c = String(floor(a * 192))
//
val rgb = "rgb("+c+","+c+","+c+")"
val ((*void*)) = ctx.fillStyle(rgb)
//
val p3 = p2+a*(p3-p2)
val p4 = p1+a*(p4-p1)
//
val () = ctx.beginPath()
val () = ctx.moveTo(p1.x(), p1.y())
val () = ctx.lineTo(p2.x(), p2.y())
val () = ctx.lineTo(p3.x(), p3.y())
val () = ctx.lineTo(p4.x(), p4.y())
val () = ctx.closePath()
//
val ((*void*)) = ctx.fill((*void*))
//
in
  // nothing
end // end of [mydraw_bargraph$fcell]
//
val () = $extfcall(void, "theCtx2d_clear")
val () = mydraw_bargraph<> (N, p1, p2, p3, p4)
//
in
  // nothing
end // end of [draw_theArray]
(**************************)
(* END: Drawing Functions *)
(**************************)

(*****************************)
(* START: Stepized Quicksort *)
(*****************************)
(* Type for quicksort steps *)

extern
fun{a:t@ype}
draw_swap(A :arrszref(a), i :int, j :int) : void = "mac#"
implement
{a}
draw_swap(A, i, j) =
let 
val () = draw_theArray(theCtx2d_get())
val tmp = A[i] in A[i] := A[j]; A[j] := tmp
end

datatype
STEP(a:t@ype) =
| DONE of ()
| QSORT of (arrszref(a), () -<cloref1> STEP(a))
| SORT of (arrszref(a), int, int, () -<cloref1> STEP(a)) // For a sorting call
| PIVOT of (arrszref(a), int, int, () -<cloref1> STEP(a)) // For choosing a pivot
| SWAP of (arrszref(a), int, int, () -<cloref1> STEP(a)) // For swapping
| PART of (arrszref(a), int, int, a, () -<cloref1> STEP(a)) // Partition 

extern
fun{a:t@ype}
quicksort_step (A :arrszref(a), k0 :() -<cloref1> STEP(a)) : STEP(a) = "mac#"
extern
fun{a:t@ype}
sort_step (A :arrszref(a), i :int, j :int, k0 :() -<cloref1> STEP(a)) : STEP(a) = "mac#"
extern
fun{a:t@ype}
pivot_step (A :arrszref(a), i :int, j :int, k0 :() -<cloref1> STEP(a)) : STEP(a) = "mac#"
extern
fun
{a:t@ype}
part_step (A :arrszref(a), i :int, j :int, pivot :a, k0 :() -<cloref1> STEP(a)) : STEP(a) = "mac#"
extern
fun
{a:t@ype}
swap_step (A :arrszref(a), i :int, j :int, k0 :() -<cloref1> STEP(a)) :STEP(a) = "mac#"

implement
{a}
quicksort_step (A, k0) =
(*
let
val a_size = g0uint2int_size_int(A.size())
in
SORT(A, 0, a_size, lam () => k0())
end
*)
SORT(A, 0, N, lam () => k0())

(* Sorting call *)
implement
{a}
sort_step (A :arrszref(a), i, j, k0) =
  if j - i >= 2 then PIVOT(A, i, j, lam () => k0()) else k0()

(* Choosing a pivot *)
(*
let
val a_size = g0uint2int_size_int(A.size())
in
SORT(A, 0, a_size, lam () => k0())
end
*)

implement
{a}
pivot_step(A, i, j, k0) =
let
  val k = i + (j-i) / 2
  val pivot = A[k]
  (* draw something *)
  val () = draw_theArray(theCtx2d_get()) 
  in 
  SWAP(A, k, j-1, lam () => PART(A, i, j-1, pivot, lam () => k0()))
end

implement
{a}
part_step (A, i, j, pivot, k0) =
let 
fun loop (i1 :int, i2 :int) : int =
(
  if i2 < j then
    if $UN.cast{int}(A[i2]) < $UN.cast{int}(pivot) then 
    let
      val () = swap(A, i1, i2)
      (* draw something *)
      val () = draw_theArray(theCtx2d_get())
    in
      loop (i1 + 1, i2 + 1)
    end else loop (i1, i2 + 1)
  else i1
)
in
  let val k2 = loop (0, 0)
  in
  SWAP(A, k2, j, lam () => SORT(A, i, k2, lam () => SORT(A, k2+1, j+1, lam () => k0())))
  end
end

(* Swaps and partition *)
implement
{a}
swap_step(A, i, j, k0) =
  let
  val () = swap(A, i, j) 
  (* draw something *)
  val () = draw_theArray(theCtx2d_get())
  in k0()
  end

(* Evaluate step into next step *)
extern
fun{a:t@ype}
step_eval
(step :STEP(a)) : STEP(a) = "mac#"

(* Turn step into stream of steps *)
extern
fun{a:t@ype}
step_meval
(step :STEP(a)) : stream_con(STEP(a)) = "mac#"

extern
fun{a:t@ype}
stepize
(A :arrszref(a)) : stream(STEP(a)) = "mac#"

extern
fun{a:t@ype}
stream_get_ans(xs :stream(STEP(a))) : void = "mac#"

extern
fun{a:t@ype}
qsort(A :arrszref(a)) : void = "mac#"


implement
{a}
stepize (A) =
$delay
(
step_meval(QSORT(A, lam () => DONE()))
)

(* Given step, evaluate it to next step *)
implement
{a}
step_eval(step) =
(
case+ step of
| DONE()                  => step 
| SORT(arr, i, j, k0)     => sort_step(arr, i, j, k0) 
| PIVOT(arr, i, j, k0)    => pivot_step(arr, i, j, k0) 
| SWAP(arr, i, j, k0)     => swap_step(arr, i, j, k0) 
| QSORT(arr, k0)          => quicksort_step(arr, k0) 
| PART(arr, i, j, pv, k0) => part_step(arr, i, j, pv, k0) 
)

(* Turn step into a stream of steps *)
implement
{a}
step_meval (step) =
stream_cons(step, $delay(step_meval(step_eval(step))))

implement
{a}
stream_get_ans(xs) =
(
case- !xs of
| stream_cons(x0, xs) =>
(
case+ x0 of
| DONE() => ()
| _      => stream_get_ans(xs)
)
)

implement
{a}
qsort(A) =
stream_get_ans(stepize(A))

extern
fun
sortArray () : void = "mac#"

implement
sortArray () = 
qsort(testArray)

(***************************)
(* END: Stepized Quicksort *)
(***************************)

(*
implement main0 () = 
{

val A = (arrszref)$arrpsz{int}(5, 20, 4, 50, 30, 2, 9, 18)
//val () = quicksort(A)
//val () = k_quicksort(A, lam () => ())
val () = qsort_int(A)
val () = print_intarray(A)

}
*)


%{$

QuickSort__dynload();
var canvas = document.getElementById("My-canvas");
var ctx2d = canvas.getContext('2d');

function
theCtx2d_get() { return ctx2d; }

function
theCtx2d_clear () 
{
  return ctx2d.clearRect(0, 0, canvas.width, canvas.height);
}

function
initize()
{
var w = canvas.width
var h = canvas.height
var w2 = 0.88 * w
var h2 = 0.88 * h

theP1_set(0, h2); theP2_set(w2, h2); theP3_set(w2, 0); theP4_set(0, 0);

ctx2d.save();
ctx2d.translate((w-w2)/2, (h-h2)/2);
return;
}

function
sort()
{
  setInterval(function(){initize();sortArray();ctx2d.restore();return;}, 400);
}


function
draw_main()
{
initize();
draw_theArray(ctx2d);
sort();
ctx2d.restore();
return;
}

jQuery(document).ready(function(){draw_main();});

%}

