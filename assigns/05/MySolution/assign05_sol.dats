(* 
Name: Andreas Singer
Course: CS520
Assignment 05
Date: 3/5/2020
*)



(* ****** ****** *)
//
#include "./../assign05.dats"
//
(* ****** ****** *)

fun
aux (d :double) : stream(double) =
$delay(
stream_cons(1./d, 
$delay (
stream_cons(~(1/(d + 1.)), aux(d + 2.))
))
)

fun
stream_ln2() : stream(double) = 
  aux(1.)

val xs2 = stream_ln2()
val-stream_cons(x', xs2) = !xs2
val () = println!("x' = ", x')
val-stream_cons(x', xs2) = !xs2
val () = println!("x' = ", x')
val-stream_cons(x', xs2) = !xs2
val () = println!("x' = ", x') 
val-stream_cons(x', xs2) = !xs2
val () = println!("x' = ", x')



(* fun
intpair_enumerate () : stream(int2) = *)



fun eTransX (n : double) (n1 : double) (n2 : double) =
  (n2 - (n2 - n1)**2) / (n + n2 - 2*n1)


fun head (xs : stream(double)) : double  =
  case+ !xs of 
  | stream_nil ()              => 0.
  | stream_cons (hd_xs, tl_xs) => hd_xs 
  
fun
EulerTrans (xs : stream(double)) : stream(double) = 
  $delay (
    let val-stream_cons(x1, xs1) = !xs in
    let val-stream_cons(x2, xs2) = !xs1 in
    let val-stream_cons(x3, xs3) = !xs2 
    in
      stream_cons((eTransX x1 x2 x3), (EulerTrans xs1))
    end
    end
    end
  )


fun 
Nats (i : double) : stream(double) =
  $delay(
    stream_cons(i, Nats(i + 1.))
  )

val xs = EulerTrans (Nats 0.)
val-stream_cons(x, xs) = !xs
val () = println!("x = ", x)
val-stream_cons(x, xs) = !xs
val () = println!("x = ", x)
val-stream_cons(x, xs) = !xs
val () = println!("x = ", x)
val-stream_cons(x, xs) = !xs
val () = println!("x = ", x)







implement main0() = ((*dummy*))

(* ****** ****** *)

(* end of [assign05_sol.dats] *)
