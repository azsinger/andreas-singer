(* ****** ****** *)
(* CS520 Assignment03
*  Name: Andreas Singer
*  Email: azsinger@bu.edu
*)
//
#include "./../assign03.dats"
//
(* ****** ****** *)
(*
Please implement isAVL2
*)
(* ****** ****** *)

fun
{a:t@ype}
isAVL2(t0: tree(a)): bool =
let
exception NONAVL of ()

fun aux(t0: tree(a)): int =
(
  case+ t0 of
  | tree_nil() => 0
  | tree_cons(tl, _, tr) =>
    let
    val htl = aux(tl)
    and htr = aux(tr)
    val dif = abs(htl - htr)
  in
    if (dif <= 1) then 1 + max(htl, htr) else $raise NONAVL()
  end
)

in
  try let
    val _ = aux(t0)
  in
    true
  end with
    ~NONAVL() => false
end

(* end of [assign03_sol.dats] *)
